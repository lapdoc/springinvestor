/*
Copyright 2010-2014 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package com.citi.trading;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
Simple REST service implementation offering CRUD methods on an
in-memory "database" of keys and values.

@author Will Provost
*/
@Controller
@RequestMapping("/Number")
public class DB
{
    private static Map<String,Integer> values =
        new HashMap<String,Integer> ();

    static
    {
        values.put ("A", 1);
        values.put ("B", 2);
        values.put ("C", 3);
        values.put ("D", 4);
        values.put ("E", 5);
        values.put ("F", 6);
        values.put ("G", 7);
        values.put ("H", 8);
        values.put ("I", 9);
        values.put ("J", 10);
    }

    /**
    Retrieves the current value for the given key, and returns it as
    a string. Fails if the key doesn't exist.
    */
    @RequestMapping(value="/{key}",method=RequestMethod.GET)
    public ResponseEntity<String> get (@PathVariable("key") String key)
    {
        System.out.println ("GET key=" + key);
        Integer value = values.get (key);
        
        return value != null
            ? new ResponseEntity<String> (value.toString (), HttpStatus.OK)
            : new ResponseEntity<String> ("", HttpStatus.NOT_FOUND);
    }

    /**
    Updates the value for the given key, and returns a string confirming
    the operation. Fails if the key doesn't exist.
    */
    @RequestMapping(value="/{key}",method=RequestMethod.PUT)
    public ResponseEntity<String> put
    (
        @PathVariable("key") String key,
        @RequestBody String value
    )
    {
        System.out.println ("PUT key=" + key + "&value=" + value);
        if (values.containsKey (key))
        {
            values.put (key, Integer.parseInt (value));
            return new ResponseEntity<String> (value, HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<String> 
            ("KEY NOT FOUND: " + key, HttpStatus.NOT_FOUND);
    }

    /**
    Adds a new key and value, and returns a string confirming
    the operation. Fails if the key already exists.
    */
    @RequestMapping(value="/{key}",method=RequestMethod.POST)
    public ResponseEntity<String> post
    (
        @PathVariable("key") String key,
        @RequestBody String value
    )
    {
        System.out.println ("POST key=" + key + "&value=" + value);
        if (!values.containsKey (key))
        {
            values.put (key, Integer.parseInt (value));
            return new ResponseEntity<String> (value, HttpStatus.CREATED);
        }

        return new ResponseEntity<String> 
            ("KEY EXISTS: " + key, HttpStatus.CONFLICT);
    }

    /**
    Deletes the given key and associated value, and returns a string confirming
    the operation. Fails if the key doesn't exist.
    */
    @RequestMapping(value="/{key}",method=RequestMethod.DELETE)
    public ResponseEntity<String> delete (@PathVariable("key") String key)
    {
        System.out.println ("DELETE key=" + key);
        if (values.containsKey (key))
        {
            values.remove (key);
            return new ResponseEntity<String> ("", HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<String> 
            ("KEY NOT FOUND: " + key, HttpStatus.NOT_FOUND);
    }
}
