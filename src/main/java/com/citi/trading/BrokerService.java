package com.citi.trading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/accounts")
public class BrokerService {

    private int identifier = 3;

	public BrokerService() {

	}

	private Map<Integer,Investor> investors = new HashMap<Integer, Investor>();

	@Autowired
	private ApplicationContext applicationContext;


	@GetMapping(produces = "application/json")
	public List<Investor> getAll () {
		List<Investor> result=new ArrayList<>();
		for (Integer id: investors.keySet()) {
			result.add(investors.get(id));
		}
		return result;
	}

	@RequestMapping(method= RequestMethod.GET, value = "{ID}", produces = "application/json")
	public ResponseEntity<Investor> getByID(@PathVariable int ID) {
		if(!investors.containsKey(ID)) {
		    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(investors.get(ID), HttpStatus.OK);
	}

    @RequestMapping(method= RequestMethod.POST,produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
	public Investor openAccount(@RequestParam double cash) {
		Investor investor = applicationContext.getBean(Investor.class);
		investor.setID(identifier++);
		investor.setCash(cash);
		investor.setPortfolio(new HashMap<String, Integer>());
		investors.put(investor.getID(), investor);
		return investor;
	}

    @RequestMapping(method= RequestMethod.DELETE,produces = "application/json")
	public ResponseEntity<Object> closeAccount(@RequestParam int ID) {
		if(!investors.containsKey(ID)) {
		    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		investors.remove(ID);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PostConstruct
	public void init() {
		Investor investor1 = applicationContext.getBean(Investor.class);
		investor1.setCash(10000);
		investor1.setID(1);
		Map<String,Integer> starter1 = new HashMap<>();
		starter1.put("MSFT", 10000);
		investor1.setPortfolio(starter1);

		Investor investor2 = applicationContext.getBean(Investor.class);
		investor1.setCash(20000);
		investor2.setID(2);
		Map<String,Integer> starter2 = new HashMap<>();
		starter2.put("ABC", 500);
		investor2.setPortfolio(starter2);

		investors.put(1,investor1);
		investors.put(2,investor2);
	}

}