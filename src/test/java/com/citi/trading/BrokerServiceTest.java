package com.citi.trading;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.InvestorTest.MockMarket;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=BrokerServiceTest.Config.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BrokerServiceTest {
	
	@Configuration
	public static class Config {
		
		@Bean
		public BrokerService mockBroker() {
			return new BrokerService();
		}
		
		@Bean
		public OrderPlacer mockMarket() {
			return new MockMarket();
		}
		
		@Bean
		@Scope("prototype")
		public Investor mockInvestor() {
			return new Investor();
		}
	}
	
	@Autowired
	private BrokerService bs;
	
	
	@Before
	public void setup() {

	}

	@Test
	public void testgetAll() {
		List<Investor> investorsList = bs.getAll();
		System.out.println(investorsList);

		assertThat(investorsList.get(0).getCash(), closeTo(20000.0, 0.0001));
		assertThat(investorsList.get(0).getPortfolio(), hasEntry(equalTo("MSFT"), equalTo(10000)));


  	    assertThat(investorsList.get(1).getCash(), closeTo(0.0, 0.0001));
		assertThat(investorsList.get(1).getPortfolio(), hasEntry(equalTo("ABC"), equalTo(500)));
	
	}

	@Test
	public void testgetId() {
		Investor investor1 = bs.getByID(1).getBody();
		assertThat(investor1.getCash(), closeTo(20000.0, 0.0001));
		assertThat(investor1.getPortfolio(), hasEntry(equalTo("MSFT"), equalTo(10000)));

		Investor investor2 = bs.getByID(2).getBody();
		assertThat(investor2.getCash(), closeTo(0.0, 0.0001));
		assertThat(investor2.getPortfolio(), hasEntry(equalTo("ABC"), equalTo(500)));
	}

	@Test
	public void testdeleteAccount() {
		bs.closeAccount(1);
		List<Investor> portfolio = bs.getAll();
		assertThat(portfolio.size(), equalTo(1));
	}

	@Test
	public void testopenAccount() {
		bs.openAccount(3);
		List<Investor> portfolio = bs.getAll();
		assertThat(portfolio.size(), equalTo(3));
	}
	
//	@Test
//	public void testgetByID() {
//		Investor investor=bs.getByID(1);
//		assertThat(investor.getCash(), closeTo(10000, 0.0001));
//		assertThat(investor.getPortfolio(), hasEntry(equalTo("MSFT"), equalTo(10000)));
//	}
	
//	@Test
//	public void testopenAccount() {
//		Investor investor=bs.openAccount(123);
//		assertThat(investor.getCash(), closeTo(123, 0.00001));
//		assertThat(investor.getPortfolio().size(), equalTo(0));	
//		assertThat(bs.getInvestors().get(3).getCash(), equalTo(123));
//	}
	
//	@Test
//	public void testcloseAccount() {
//		Investor investor1=bs.openAccount(123);
//		Investor investor2=bs.openAccount(123);
//		
//
//
//	}
	

	

}
